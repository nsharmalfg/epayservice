package com.lfg.hub.secure.epayservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpayServiceApplication.class, args);
		//test
	}

}
